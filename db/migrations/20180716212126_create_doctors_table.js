
exports.up = function(knex, Promise) {
    return knex.schema.createTable('doctors', function(table) {
        table.charset('utf8mb4')
        table.collate('utf8mb4_unicode_ci')

        table.increments('id').primary()
        table.string('first_name', 100).notNullable()
        table.string('last_name', 100).notNullable()
        table.string('email', 100).nullable()
        table.string('address').nullable()
        table.string('phone').notNullable()
        table.string('idnp', 20).nullable()
        table.text('comment').nullable()
        table.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('doctors')
};
