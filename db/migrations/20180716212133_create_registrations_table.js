
exports.up = function(knex, Promise) {
    return knex.schema.createTable('registrations', function(table) {
        table.charset('utf8mb4')
        table.collate('utf8mb4_unicode_ci')

        table.increments('id').primary()
        table.text('pacient_name')
        table.text('pacient_phone')
        table.text('pacient_email')
        table.integer('doctor_id').unsigned().references('doctors.id')
        table.integer('procedure_id').unsigned().references('procedures.id')
        table.dateTime('start_at')
        table.dateTime('end_at')
        table.specificType('count_person', 'SMALLINT DEFAULT 0')
        table.text('comment').nullable()
        table.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('registrations')
};
