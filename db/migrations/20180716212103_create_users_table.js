
exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', function(table) {
        table.charset('utf8mb4')
        table.collate('utf8mb4_unicode_ci')

        table.increments('id').primary()
        table
            .string('username', 100)
            .notNullable()
            .unique()
        table.string('password').notNullable()
        table.string('email', 100).nullable()
        table.string('phone', 100).nullable()
        table.specificType('telegram_bot_id', 'CHAR(20) DEFAULT NULL').nullable()
        table.boolean('telegram_message_received').defaultTo(true)
        table.string('token').nullable()
        table.timestamps(false, true)
    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('users')
};
