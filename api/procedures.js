
const db = require('../db')
const express = require('express')
const bodyParser = require('body-parser')

// Create app
const app = express()
app.use(bodyParser.json())

// -- Routes --
// [GET] /
app.get('/', async (req, res, next) => {
  var procedures = await db('procedures')
  res.json(procedures)
})

// [POST]
app.post('/', async (req, res, next) => {
  const { procedure } = req.body

  var procedures = await db('procedures').insert(procedure)
  res.json({id: procedures[0]})
})

app.put('/:id', async (req, res, next) => {
  const { procedure } = req.body

  var res_procedure = await db('procedures').where('id', req.params.id).update(procedure)

  res.json({id: res_procedure[0]})
})

app.delete('/:id', async (req, res, next) => {

  await db('procedures').where('id', req.params.id).del()

  res.json({'status': 'ok'})
})

// Error handler
app.use((err, req, res, next) => {
  console.error(err) // eslint-disable-line no-console
  res.status(401).send(err + '')
})

// -- export app --
module.exports = {
  path: '/api/procedures',
  handler: app
}
