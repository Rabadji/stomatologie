
const db = require('../db')
const express = require('express')
const bodyParser = require('body-parser')

// Create app
const app = express()
app.use(bodyParser.json())




// [GET] /
app.get('/', async (req, res, next) => {
  var registrations = await db('registrations').select('registrations.id', 'registrations.pacient_name', 'registrations.pacient_email','registrations.pacient_phone','registrations.doctor_id', 'registrations.procedure_id', 'registrations.comment', 'registrations.start_at AS start', 'registrations.end_at AS end', 'registrations.created_at', db.raw("CONCAT(doctors.first_name, ' ', doctors.last_name) AS doctor_name"), 'doctors.phone AS doctor_phone', 'doctors.email AS doctor_email','doctors.idnp AS doctor_idnp', 'doctors.address AS doctor_address', 'procedures.name AS procedure_name')
                      .innerJoin('doctors', 'doctors.id', 'registrations.doctor_id')
                      .leftJoin('procedures', 'procedures.id', 'registrations.procedure_id')
                         .orderBy('registrations.start_at', 'DESC')
  res.json(registrations)
})

// [POST]
app.post('/', async (req, res, next) => {
  const { registration } = req.body

  var res_registration = await db('registrations').insert(registration)
  res.json({id: res_registration[0]})
})

app.put('/:id', async (req, res, next) => {
  const { registration } = req.body

  var res_registration = await db('registrations').where('id', req.params.id).update(registration)

  res.json({id: res_registration[0]})
})


// Error handler
app.use((err, req, res, next) => {
  console.error(err) // eslint-disable-line no-console
  res.status(401).send(err + '')
})

// -- export app --
module.exports = {
  path: '/api/registrations',
  handler: app
}
