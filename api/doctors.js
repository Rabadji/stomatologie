
const db = require('../db')
const express = require('express')
const bodyParser = require('body-parser')

// Create app
const app = express()
app.use(bodyParser.json())

// -- Routes --
// [GET] /
app.get('/', async (req, res, next) => {
  var doctors = await db('doctors')
  res.json(doctors)
})

// [POST]
app.post('/', async (req, res, next) => {
  const { doctor } = req.body

  var doctors = await db('doctors').insert(doctor)
  res.json({id: doctors[0]})
})

app.put('/:id', async (req, res, next) => {
  const { doctor } = req.body

  var res_doctor = await db('doctors').where('id', req.params.id).update(doctor)

  res.json({id: res_doctor[0]})
})

app.delete('/:id', async (req, res, next) => {

  await db('doctors').where('id', req.params.id).del()

  res.json({'status': 'ok'})
})

// Error handler
app.use((err, req, res, next) => {
  console.error(err) 
  res.status(401).send(err + '')
})

// -- export app --
module.exports = {
  path: '/api/doctors',
  handler: app
}
