
const db = require('../db')
const express = require('express')
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const crypto = require('crypto')

// Create app
const app = express()
app.use(bodyParser.json())

// -- Routes --
// [GET] /
app.get('/', async (req, res, next) => {
  var users = await db('users').select('id', 'username', 'email', 'phone', 'telegram_bot_id', 'telegram_message_received', 'created_at')

  res.json(users)
})

// [POST]
app.post('/', async (req, res, next) => {
  const { user } = req.body

  delete user.confirm_password
  var hash = bcrypt.hashSync(user.password, 10)
  var randomToken = crypto.randomBytes(48).toString('hex')
  user.password = hash
  user.token = randomToken

  var users = await db('users').insert(user)
  res.json({id: users[0]})
})

app.put('/:id', async (req, res, next) => {
  const { user } = req.body
  delete user.confirm_password
  if (user.password && user.password != '') {
    var hash = bcrypt.hashSync(user.password, 10)
    var randomToken = crypto.randomBytes(48).toString('hex')
    user.password = hash
    user.token = randomToken
  } else {
    delete user.password;
  }

  var res_user = await db('users').where('id', req.params.id).update(user)

  res.json({id: res_user[0]})
})

app.delete('/:id', async (req, res, next) => {

  await db('users').where('id', req.params.id).del()

  res.json({'status': 'ok'})
})

// Error handler
app.use((err, req, res, next) => {
  console.error(err) // eslint-disable-line no-console
  res.status(401).send(err + '')
})

// -- export app --
module.exports = {
  path: '/api/users',
  handler: app
}
