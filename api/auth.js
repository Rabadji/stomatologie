const db = require('../db')

const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const jwt = require('express-jwt')
const jsonwebtoken = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const crypto = require('crypto')

// Create app
const app = express()

// Install middleware
app.use(cookieParser())
app.use(bodyParser.json())

// JWT middleware
app.use(
  jwt({
    secret: 'dummy'
  }).unless({
    path: '/api/auth/login'
  })
)

// -- Routes --

// [POST] /login
app.post('/login', async (req, res, next) => {
  const { username, password } = req.body
  if (!username || !password)
    return res.status(400).json({type: 'error', message: 'Поля "Логин" и "Пароль" должны быть заполнены.'})

  let [check] = await db('users').count('id as id')

  if (!check.id) {
    var hash = bcrypt.hashSync('admin!', 10);
    var randomToken = crypto.randomBytes(48).toString('hex');
    let q_res = await db('users').insert({username: 'admin', password: hash, token: randomToken})
  }

  var [userData] = await db('users').where({ username: username })

  if (!userData)
    return res.status(403).json({type: 'error', message: 'Пользователь с таким Логином не найден.'})

  let correct = await bcrypt.compare(password, userData.password)
  if (!correct)
      return res.status(403).json({type: 'error', message: 'Неверный Пароль.'})

  const accessToken = jsonwebtoken.sign(
    {
      username,
      name: 'Пользователь ' + username,
      scope: ['admin']
    },
    'dummy'
  )

  res.json({
    token: {
      accessToken
    }
  })

})

// [GET] /user
app.get('/user', (req, res, next) => {
  res.json({ user: req.user })
})

// [POST] /logout
app.post('/logout', (req, res, next) => {
  res.json({ status: 'OK' })
})

// Error handler
app.use((err, req, res, next) => {
  console.error(err) // eslint-disable-line no-console
  res.status(401).send(err + '')
})

// -- export app --
module.exports = {
  path: '/api/auth',
  handler: app
}
