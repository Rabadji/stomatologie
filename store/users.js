import axios from '~/plugins/axios'
import moment from 'moment'
var _ = require('lodash/core');

const user_struct = {
    id: null,
    username: null,
    password: null,
    email: null,
    phone: null,
    telegram_bot_id: null,
    telegram_message_received: null,
}

export const state = () => ({
  list: [],
  user: user_struct,
  status: {
    loading: false,
    success: false,
    error: false
  },
  addUser: false,
  listUser: false,
  editUser: false,
  editting: false,
})

export const getters = {
  user (state) {
    return state.user
  },
  addUser (state) {
    return state.addUser
  },
  editUser (state) {
    return state.editUser
  },
  listUser (state) {
    return state.listUser
  },
  status (state) {
    return state.status
  },
  editting (state) {
    return state.editting
  },
}

export const mutations = {
  setList(state, lists) {
    if (!_.isArray(lists))
      lists = [lists]
    state.list = lists
  },

  addUser(state, payload) {
    state.list.push(payload)
  },
  updateUser(state, payload) {
    state.list.splice(state.list.indexOf(payload), 1, payload)
  },
  removeUser(state, client) {
    state.list.splice(state.list.indexOf(client), 1)
  },

  TOGGLE_ADD_USER (state) {
    state.addUser = !state.addUser
    if (!state.addUser) {
      state.user = user_struct
      state.editting = false
    }
  },
  TOGGLE_LIST_USER (state) {
    state.listUser = !state.listUser
  },
  TOGGLE_EDITTING (state, payload) {
    state.editting = payload
  },
  SET_USER (state, payload) {
    state.user = payload
  },

  LOADING (state) {
    state.status = {
      loading: true,
      success: false,
      error: false
    }
  },
  SUCCESS (state) {
    state.status = {
      loading: false,
      success: true,
      error: false
    }
  },
  ERROR (state, payload) {
    state.status = {
      loading: false,
      success: false,
      error: payload
    }
  },

}

export const actions = {
  async loadUsers({ commit }) {
    commit('LOADING')
    await axios.get('/api/users').then(response => {
      commit('setList', response.data)
    })
    commit('SUCCESS')
  },
  toggleAddUser (context) {
    context.commit('TOGGLE_ADD_USER')
  },
  addUser (context, payload) {
    context.commit('LOADING')
    axios.post('/api/users', {user: payload}).then(response => {
      payload.id = response.data.id
      payload.created_at = new Date()
      context.commit('TOGGLE_ADD_USER')
      context.commit('addUser', payload)
      context.commit('SUCCESS')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  editUser (context, payload) {
    context.commit('LOADING')
    axios.put('/api/users/' + payload.id, {user: payload}).then(response => {
      context.commit('TOGGLE_ADD_USER')
      context.commit('TOGGLE_EDITTING', false)
      context.commit('updateUser', payload)
      context.commit('SUCCESS')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  removeUser (context, payload) {
    axios.delete('/api/users/' + payload.id).then(response => {
      context.commit('removeUser', payload)
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  toggleListUser (context) {
    context.commit('TOGGLE_LIST_USER')
  },
  setUser (context, payload) {
    context.commit('SET_USER', payload)
  },
  toggleEditting (context, payload) {
    context.commit('TOGGLE_EDITTING', payload)
  },
}
