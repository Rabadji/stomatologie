import axios from '~/plugins/axios'
import moment from 'moment'
var _ = require('lodash/core');

const procedure_struct = {
    id: null,
    name: null,
    comment: null,
}

export const state = () => ({
  list: [],
  procedure: procedure_struct,
  status: {
    loading: false,
    success: false,
    error: false
  },
  addProcedure: false,
  listProcedure: false,
  editProcedure: false,
  editting: false,
})

export const getters = {
  procedure (state) {
    return state.procedure
  },
  addProcedure (state) {
    return state.addProcedure
  },
  editProcedure (state) {
    return state.editProcedure
  },
  listProcedure (state) {
    return state.listProcedure
  },
  status (state) {
    return state.status
  },
  editting (state) {
    return state.editting
  },
}

export const mutations = {
  setList(state, lists) {
    if (!_.isArray(lists))
      lists = [lists]
    state.list = lists
  },

  addProcedure(state, payload) {
    state.list.push(payload)
  },
  updateProcedure(state, payload) {
    state.list.splice(state.list.indexOf(payload), 1, payload)
  },
  removeProcedure(state, procedure) {
    state.list.splice(state.list.indexOf(procedure), 1)
  },

  TOGGLE_ADD_PROCEDURE (state) {
    state.addProcedure = !state.addProcedure
    if (!state.addProcedure) {
      state.procedure = procedure_struct
      state.editting = false
    }
  },
  TOGGLE_LIST_PROCEDURE (state) {
    state.listProcedure = !state.listProcedure
  },
  TOGGLE_EDITTING (state, payload) {
    state.editting = payload
  },
  SET_PROCEDURE (state, payload) {
    state.procedure = payload
  },

  LOADING (state) {
    state.status = {
      loading: true,
      success: false,
      error: false
    }
  },
  SUCCESS (state) {
    state.status = {
      loading: false,
      success: true,
      error: false
    }
  },
  ERROR (state, payload) {
    state.status = {
      loading: false,
      success: false,
      error: payload
    }
  },

}

export const actions = {
  async loadProcedures({ commit }) {
    commit('LOADING')
    await axios.get('/api/procedures').then(response => {
      commit('setList', response.data)
    })
    commit('SUCCESS')
  },
  toggleAddProcedure (context) {
    context.commit('TOGGLE_ADD_PROCEDURE')
  },
  addProcedure (context, payload) {
    context.commit('LOADING')
    axios.post('/api/procedures', {procedure: payload}).then(response => {
      payload.id = response.data.id
      payload.created_at = new Date()
      context.commit('TOGGLE_ADD_PROCEDURE')
      context.commit('addProcedure', payload)
      context.commit('SUCCESS')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  editProcedure (context, payload) {
    context.commit('LOADING')
    axios.put('/api/procedures/' + payload.id, {procedure: payload}).then(response => {
      context.commit('TOGGLE_ADD_PROCEDURE')
      context.commit('TOGGLE_EDITTING', false)
      context.commit('updateProcedure', payload)
      context.commit('SUCCESS')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  removeProcedure (context, payload) {
    axios.delete('/api/procedures/' + payload.id).then(response => {
      context.commit('removeProcedure', payload)
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  toggleListProcedure (context) {
    context.commit('TOGGLE_LIST_PROCEDURE')
  },
  setProcedure (context, payload) {
    context.commit('SET_PROCEDURE', payload)
  },
  toggleEditting (context, payload) {
    context.commit('TOGGLE_EDITTING', payload)
  },
}
