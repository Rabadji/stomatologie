export const strict = false

export const actions = {
    async nuxtServerInit({ dispatch }, { req }) {
      if (this.state.auth.loggedIn) {
        await dispatch('registrations/loadRegistrations')
        await dispatch('doctors/loadDoctors')
        await dispatch('procedures/loadProcedures')
        await dispatch('users/loadUsers')
      }
    }
}
