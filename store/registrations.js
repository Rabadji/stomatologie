import axios from '~/plugins/axios'
import moment from 'moment'
var _ = require('lodash/core');

const registration_struct = {
    id: null,
    pacient_name: null,
    pacient_phone: null,
    pacient_email: null,
    doctor_id: null,
    procedure_id: null,
    comment: null,
    metadata: {
      start_date: null,
      start_time: null,
      end_date:null,
      end_time:null,
    }
}

export const state = () => ({
  list: [],
  status_list: [],
  registration: registration_struct,
  status: {
    loading: false,
    success: false,
    error: false
  },
  addRegistration: false,
  editting: false,
})

export const getters = {
  registration (state) {
    return state.registration
  },
  addRegistration (state) {
    return state.addRegistration
  },
  status (state) {
    return state.status
  },
  editting (state) {
    return state.editting
  },
}

export const mutations = {
  setList(state, lists) {
    if (!_.isArray(lists))
      lists = [lists]
    state.list = lists
  },
  setStatusList(state, lists) {
    if (!_.isArray(lists))
      lists = [lists]
    state.status_list = lists
  },

  addRegistration(state, payload) {
    state.list.push(payload)
  },
  updateRegistration(state, payload) {
    state.list.splice(state.list.indexOf(payload), 1, payload)
  },

  remove(state, registration) {
    state.list.splice(state.list.indexOf(registration), 1)
  },

  TOGGLE_ADD_REGISTRATION (state) {
    state.addRegistration = !state.addRegistration
    if (!state.addRegistration) {
      state.registration = registration_struct
      state.editting = false
    }
  },
  TOGGLE_EDITTING (state, payload) {
    state.editting = payload
  },
  SET_REGISTRATION (state, payload) {
    state.registration = payload
  },

  LOADING (state) {
    state.status = {
      loading: true,
      success: false,
      error: false
    }
  },
  SUCCESS (state) {
    state.status = {
      loading: false,
      success: true,
      error: false
    }
  },
  ERROR (state, payload) {
    state.status = {
      loading: false,
      success: false,
      error: payload
    }
  },

}

export const actions = {
  async loadRegistrations({ commit }) {
    commit('LOADING')
    await axios.get('/api/registrations').then(response => {
      commit('setList', response.data)
    })
    commit('SUCCESS')
  },
  async loadStatuses({ commit }) {
    commit('LOADING')
    await axios.get('/api/registrations/statuses').then(response => {
      commit('setStatusList', response.data)
    })
    commit('SUCCESS')
  },
  toggleAddRegistration (context) {
    context.commit('TOGGLE_ADD_REGISTRATION')
    if (!context.state.addRegistration) {
      context.dispatch('loadRegistrations')
    }
  },
  addRegistration (context, payload) {
    context.commit('LOADING')
    const registration = {
      pacient_name:payload.pacient_name,
      pacient_phone: payload.pacient_phone,
      pacient_email:payload.pacient_email,
      doctor_id: payload.doctor_id,
      procedure_id: payload.procedure_id,
      comment: payload.comment,
      start_at: moment(payload.metadata.start_date).format('YYYY-MM-DD') + ' ' + moment(payload.metadata.start_time).format('HH:mm'),
      end_at: moment(payload.metadata.start_date).format('YYYY-MM-DD') + ' ' + moment(payload.metadata.start_time.getTime() + (1*60*60*1000)).format('HH:mm'),
    }
    axios.post('/api/registrations', {registration: registration}).then(response => {
      payload.id = response.data.id
      payload.start = registration.start_at
      payload.end = registration.end_at

      context.commit('TOGGLE_ADD_REGISTRATION')
      //context.commit('addRegistration', payload)
      context.commit('SUCCESS')
      context.dispatch('loadRegistrations')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  editRegistration (context, payload) {
    context.commit('LOADING')
    const registration = {
      id: payload.id,
      pacient_name:payload.pacient_name,
      pacient_phone: payload.pacient_phone,
      pacient_email:payload.pacient_email,
      doctor_id: payload.doctor_id,
      procedure_id: payload.procedure_id,
     
      
      comment: payload.comment,
      start_at: moment(payload.metadata.start_date).format('YYYY-MM-DD') + ' ' + moment(payload.metadata.start_time).format('HH:mm'),
      end_at: moment(payload.metadata.start_date).format('YYYY-MM-DD') + ' ' + moment(payload.metadata.start_time.getTime() + (1*60*60*1000)).format('HH:mm'),
    }
    axios.put('/api/registrations/' + payload.id, {registration: registration}).then(async response => {
      payload.start = registration.start_at
      payload.end = registration.end_at
      if (context.state.editting) {
        context.commit('TOGGLE_ADD_REGISTRATION')
        context.commit('TOGGLE_EDITTING', false)
        //context.commit('updateRegistration', payload)
        await axios.get('/api/registrations/set-status/' + payload.id).then(response => {
          //commit('setStatusList', response.data)
        })
      }
      context.commit('SUCCESS')
      context.dispatch('loadRegistrations')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  removeRegistration (context, payload) {
    let status = _.find(context.state.status_list, {code: 0})
    const registration = {
      status_id: status.id
    }
    axios.put('/api/registrations/' + payload.id, {registration: registration}).then(response => {
      context.commit('TOGGLE_ADD_REGISTRATION')
      context.commit('TOGGLE_EDITTING', false)
      context.commit('SUCCESS')
      context.dispatch('loadRegistrations')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  restoreRegistration (context, payload) {
    let status = _.find(context.state.status_list, {code: 1})
    const registration = {
      status_id: status.id
    }
    axios.put('/api/registrations/' + payload.id, {registration: registration}).then(response => {
      context.commit('TOGGLE_ADD_REGISTRATION')
      context.commit('TOGGLE_EDITTING', false)
      context.commit('SUCCESS')
      context.dispatch('loadRegistrations')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  setRegistration (context, payload) {
    context.commit('SET_REGISTRATION', payload)
  },
  toggleEditting (context, payload) {
    context.commit('TOGGLE_EDITTING', payload)
  },
}
