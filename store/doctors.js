import axios from '~/plugins/axios'
import moment from 'moment'
var _ = require('lodash/core');

const doctor_struct = {
    id: null,
    first_name: null,
    last_name: null,
    email: null,
    phone: null,
    address: null,
    idnp: null,
    comment: null,
}

export const state = () => ({
  list: [],
  doctor: doctor_struct,
  status: {
    loading: false,
    success: false,
    error: false
  },
  addDoctor: false,
  listDoctor: false,
  editDoctor: false,
  editting: false,
})

export const getters = {
  doctor (state) {
    return state.doctor
  },
  addDoctor (state) {
    return state.addDoctor
  },
  editDoctor (state) {
    return state.editDoctor
  },
  listDoctor (state) {
    return state.listDoctor
  },
  status (state) {
    return state.status
  },
  editting (state) {
    return state.editting
  },
}

export const mutations = {
  setList(state, lists) {
    if (!_.isArray(lists))
      lists = [lists]
    state.list = lists
  },

  addDoctor(state, payload) {
    state.list.push(payload)
  },
  updateDoctor(state, payload) {
    state.list.splice(state.list.indexOf(payload), 1, payload)
  },
  removeDoctor(state, doctor) {
    state.list.splice(state.list.indexOf(doctor), 1)
  },

  TOGGLE_ADD_DOCTOR (state) {
    state.addDoctor = !state.addDoctor
    if (!state.addDoctor) {
      state.doctor = doctor_struct
      state.editting = false
    }
  },
  TOGGLE_LIST_DOCTOR (state) {
    state.listDoctor = !state.listDoctor
  },
  
  TOGGLE_EDITTING (state, payload) {
    state.editting = payload
  },
  SET_DOCTOR (state, payload) {
    state.doctor = payload
  },

  LOADING (state) {
    state.status = {
      loading: true,
      success: false,
      error: false
    }
  },
  SUCCESS (state) {
    state.status = {
      loading: false,
      success: true,
      error: false
    }
  },
  ERROR (state, payload) {
    state.status = {
      loading: false,
      success: false,
      error: payload
    }
  },

}

export const actions = {
  async loadDoctors({ commit }) {
    commit('LOADING')
    await axios.get('/api/doctors').then(response => {
      commit('setList', response.data)
    })
    commit('SUCCESS')
  },
  toggleAddDoctor (context) {
    context.commit('TOGGLE_ADD_DOCTOR')
  },
  addDoctor (context, payload) {
    context.commit('LOADING')
    axios.post('/api/doctors', {doctor: payload}).then(response => {
      payload.id = response.data.id
      payload.created_at = new Date()
      context.commit('TOGGLE_ADD_DOCTOR')
      context.commit('addDoctor', payload)
      context.commit('SUCCESS')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  editDoctor (context, payload) {
    context.commit('LOADING')
    axios.put('/api/doctors/' + payload.id, {doctor: payload}).then(response => {
      context.commit('TOGGLE_ADD_DOCTOR')
      context.commit('TOGGLE_EDITTING', false)
      context.commit('updateDoctor', payload)
      context.commit('SUCCESS')
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  removeDoctor (context, payload) {
    axios.delete('/api/doctors/' + payload.id).then(response => {
      context.commit('removeDoctor', payload)
    }).catch(e => {
      context.commit('ERROR', 'ОШИБКА!')
    })
  },
  toggleListDoctor (context) {
    context.commit('TOGGLE_LIST_DOCTOR')
  },
 
  setDoctor (context, payload) {
    context.commit('SET_DOCTOR', payload)
  },
  toggleEditting (context, payload) {
    context.commit('TOGGLE_EDITTING', payload)
  },
}
