module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'TStomatologie',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'TStomatologie' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['moment', 'axios', 'vue-full-calendar','vueditor'],
    extractCSS: true,
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    },
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      config.node = {
        fs: 'empty'
      }
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    }
  },
  serverMiddleware: [
    '~/api/auth',
    '~/api/users',
    '~/api/registrations',
    '~/api/doctors',
    '~/api/procedures',
   
  ],
  router: {
    middleware: ['auth']
  },
  plugins: [
 
   
    { src: '~plugins/vue-full-calendar', ssr: false },
    '~plugins/vue-async-computed',
    '~plugins/vee-validate',
    '~plugins/buefy',
    '~plugins/v-money',
    '~plugins/v-tooltip',
   
   
  ],
  css: [
    'fullcalendar/dist/fullcalendar.css',
    'vueditor/dist/style/vueditor.min.css',
    'mdi/css/materialdesignicons.css',
    '@fortawesome/fontawesome-free/css/all.css',
    '@/assets/styles/app.scss',
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/dotenv',
  ],
  axios: {
    proxy: true
  },
  proxy: {
    '/api': 'http://localhost:3000'
  },
  auth: {
    // Options
    redirect: {
      callback: '/callback',
      logout: '/login',
    },
    strategies: {
      local: {
        endpoints: {
          login: { propertyName: 'token.accessToken' }
        }
      }
    }
  }
}
