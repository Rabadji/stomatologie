import Vue from 'vue'
import VueFullCalendar from 'vue-full-calendar'
require('fullcalendar/dist/locale/ru.js')

Vue.use(VueFullCalendar)
