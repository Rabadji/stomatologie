import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'

import moment from 'moment'

Vue.use(Buefy, {
  defaultDateFormatter: (date) => moment(date).format('DD/MM/YYYY')
})
